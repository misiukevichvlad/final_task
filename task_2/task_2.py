import requests
import json

with open('data_file.json', 'r') as f:
    data = json.load(f)
for i in data:
    if i['Link']:
        url = i['Link']
    if i['Token']:
        token = i['Token']
    if i['DATA']:
        data = i['DATA']
    if i['DATA2']:
        data2 = i['DATA2']


def get_users():
    response = requests.get(f'{url}/public-api/users?access-token={token}').json()
    return response


def get_user_number_13():
    response = requests.get(f'{url}/public-api/users/13?access-token={token}').json()
    return response


def get_post_5():
    response = requests.get(f'{url}/public-api/posts/5?access-token={token + "k"}').json()
    return response


def create_user1():
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {token}'
    }
    response = requests.post(f'{url}/public-api/users', headers=headers, json=data).json()
    return response


def update_user1():
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {token}'
    }
    response = requests.put(f'{url}/public-api/users/1990', headers=headers,
                            json={"first_name": "gena"}).json()
    return response


def create_user2():
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {token}'
    }
    response = requests.post(f'{url}/public-api/users', headers=headers, json=data2).json()
    return response


def delete_user():
    response = requests.delete(f'{url}/public-api/users/1983?access-token={token}').json()
    return response


def delete_user2():
    response = requests.delete(f'{url}/public-api/users/2842?access-token={token}').json()
    return response
