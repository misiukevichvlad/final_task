import json
import task_2


def returning_correct_status_cod(data):
    for field in data:
        if field == '_meta':
            for i in data[field]:
                if i == 'code':
                    return data[field][i]


def returning_user_email(data):
    for field in data:
        if field == 'result':
            for i in data[field]:
                if i == 'email':
                    return data[field][i]


def returning_entered_email():
    with open('data_file.json', 'r') as f:
        data1 = json.load(f)
    for i in data1:
        if 'email' in i['DATA']:
            return i['DATA']['email']


def test_users_are_get():
    get1 = task_2.get_users()
    code = returning_correct_status_cod(get1)
    assert code == 200, 'Error'


def test_users_13():
    get2 = task_2.get_user_number_13()
    code = returning_correct_status_cod(get2)
    assert code == 200, 'Error'


def test_request_with_incorrect_token():
    get3 = task_2.get_post_5()
    code = returning_correct_status_cod(get3)
    assert code == 401, 'Error'


def test_user1_by_email():
    post1 = task_2.create_user1()
    code = returning_correct_status_cod(post1)
    assert code == 200, 'Error'
    email = returning_user_email(post1)
    entered_email = returning_entered_email()
    assert email == entered_email, 'Error'


def test_updating_user1():
    put1 = task_2.update_user1()
    code = returning_correct_status_cod(put1)
    assert code == 200, 'Error'


def test_user_delete():
    delete1 = task_2.delete_user()
    code = returning_correct_status_cod(delete1)
    assert code == 204, 'Error'


def test_delete_unexistied_user():
    delete2 = task_2.delete_user2()
    code = returning_correct_status_cod(delete2)
    assert code != 204, 'Error'


def test_creating_user2():
    post2 = task_2.create_user2()
    code = returning_correct_status_cod(post2)
    assert code != 400, 'Error'
