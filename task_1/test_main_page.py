from pages.basket_page import BasketPage
from pages.login_page import LoginPage
from pages.main_page import MainPage
import json

with open('data.json', 'r') as f:
    data = json.load(f)
for i in data:
    if i['URL']:
        url = i['URL']


def test_is_it_main_page(browser):
    page = MainPage(browser, url)
    main_page = page.open_main_page()
    main_page.should_be_elem_on_main_page()
    main_page.should_be_main_page()


def test_is_it_login_page(browser):
    page = LoginPage(browser, url)
    page.open()
    login_page = page.open_login_page()
    login_page.should_be_login_page()


def test_is_it_basket_page(browser):
    page = BasketPage(browser, url)
    page.open()
    basket_page = page.open_basket_page()
    basket_page.should_be_basket_page()


def test_user_can_open_login_page(browser):
    page = LoginPage(browser, url)
    page.open()
    page.open_login_page()
    page.should_be_elem_on_login_page()


def test_is_basket_empty(browser):
    page = BasketPage(browser, url)
    page.open()
    page.open_basket_page()
    page.basket_should_be_empty()


def test_user_can_log_in(browser):
    page = LoginPage(browser, url)
    page.open()
    page.open_login_page()
    page.should_be_email_element()
    page.enter_email()
    page.should_be_password_element()
    page.enter_password()
    page.should_be_button_log_in()
    page.try_to_log_in()
    page.check_user_log_in()


def test_product_is_in_basket(browser):
    page = MainPage(browser, url)
    page.open()
    page.should_be_button_add_to_cart()
    page.check_is_product_in_basket()
