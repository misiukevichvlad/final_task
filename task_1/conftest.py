from selenium import webdriver
from pytest import fixture


@fixture()
def browser():
    print("Start browser")
    driver = webdriver.Chrome('C:\chromedriver\chromedriver.exe')
    yield driver
    print('Quit browser')
    driver.quit()
