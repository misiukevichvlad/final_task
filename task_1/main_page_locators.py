from selenium.webdriver.common.by import By


class MainPageLocators:
    ELEM_LINK = (By.XPATH, '//h1[text()="Automation Practice Website"]')
    LOGIN_LINK = (By.CLASS_NAME, 'login')
    Elem_on_login_page = (By.CLASS_NAME, 'page-heading')
    BASKET_LINK = (By.XPATH, '//b[text()="Cart"]')
    Empty_Basket = (By.XPATH, '//p[text()="Your shopping cart is empty."]')
    Email_address = (By.CSS_SELECTOR, '#email')
    Password = (By.CSS_SELECTOR, '#passwd')
    Button_to_log_in = (By.CSS_SELECTOR, '#SubmitLogin')
    User_name = (By.XPATH, '//span[text()="Vlad Misiukevich"]')
    Button_add_to_card = (By.CSS_SELECTOR, '.product_list:nth-child(1)'
                                           ' .ajax_block_product:nth-child(1)'
                                           ' .button:nth-child(1)')
    Check_the_product_is_add = (By.CLASS_NAME, 'icon-ok')
