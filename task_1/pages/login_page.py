import json
import time
from pages.base_page import BasePage
from locators.main_page_locators import MainPageLocators

with open('data.json', 'r') as f:
    data = json.load(f)
for i in data:
    if i['Link']:
        link = i['Link']
    if i['Email address']:
        email_addr = i['Email address']
    if i['Password']:
        password = i['Password']


class LoginPage(BasePage):
    def open_login_page(self):
        login_link = self.driver.find_element(*MainPageLocators.LOGIN_LINK)
        login_link.click()
        return LoginPage(driver=self.driver, url=self.driver.current_url)

    def should_be_login_page(self):
        assert self.driver.current_url == link

    def should_be_elem_on_login_page(self):
        time.sleep(1.5)
        assert self.is_elem_present(*MainPageLocators.Elem_on_login_page), 'No such element'

    def should_be_email_element(self):
        time.sleep(1)
        assert self.is_elem_present(*MainPageLocators.Email_address), 'No such element'

    def enter_email(self):
        email_link = self.driver.find_element(*MainPageLocators.Email_address)
        email_link.send_keys(email_addr)

    def should_be_password_element(self):
        assert self.is_elem_present(*MainPageLocators.Password), 'No such element'

    def enter_password(self):
        password_link = self.driver.find_element(*MainPageLocators.Password)
        password_link.send_keys(password)

    def should_be_button_log_in(self):
        assert self.is_elem_present(*MainPageLocators.Button_to_log_in), 'No such element'

    def try_to_log_in(self):
        button_log = self.driver.find_element(*MainPageLocators.Button_to_log_in)
        button_log.click()

    def check_user_log_in(self):
        time.sleep(1)
        assert self.is_elem_present(*MainPageLocators.User_name), 'No such element'
