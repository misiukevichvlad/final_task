import json
from pages.base_page import BasePage
from locators.main_page_locators import MainPageLocators

with open('data.json', 'r') as f:
    data = json.load(f)
for i in data:
    if i['Link1']:
        link1 = i['Link1']


class MainPage(BasePage):
    def should_be_elem_on_main_page(self):
        assert self.is_elem_present(*MainPageLocators.ELEM_LINK), 'No such element'

    def open_main_page(self):
        self.driver.get(self.url)
        return MainPage(driver=self.driver, url=self.driver.current_url)

    def should_be_main_page(self):
        assert self.driver.current_url == link1

    def should_be_button_add_to_cart(self):
        assert self.is_elem_present(*MainPageLocators.Button_add_to_card), 'No such element'

    def add_product_to_basket(self):
        add_product = self.driver.find_element(*MainPageLocators.Button_add_to_card)
        add_product.click()

    def check_is_product_in_basket(self):
        assert self.is_elem_present(*MainPageLocators.Check_the_product_is_add), 'No such element'
