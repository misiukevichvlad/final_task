import json
import time
from pages.base_page import BasePage
from locators.main_page_locators import MainPageLocators

with open('data.json', 'r') as f:
    data = json.load(f)
for i in data:
    if i['Link2']:
        link_2 = i['Link2']


class BasketPage(BasePage):
    def open_basket_page(self):
        basket_link = self.driver.find_element(*MainPageLocators.BASKET_LINK)
        basket_link.click()
        return BasketPage(driver=self.driver, url=self.driver.current_url)

    def should_be_basket_page(self):
        assert self.driver.current_url == link_2

    def basket_should_be_empty(self):
        time.sleep(1)
        assert self.is_elem_present(*MainPageLocators.Empty_Basket), 'No such element'
